# 微信小程序

黑马程序员出品

## 我们能学会什么？

### 🎯 小程序基础

组件、配置、生命周期、事件处理、数据渲染、API、自定义组件、Vant UI、分包加载等

### 🥇 享+生活项目

通告管理、用户管理、房屋管理、报修管理、访客管理等各个功能模块

**技术和功能点**

- Vant 组件库
- 短信验证码
- Refresh Token
- 状态管理
- 地理定位
- 逆地址解析
- 地点搜索
- 路线规划
- 文件上传
- 自定义分享
- 相册访问
- ...

### 🥈 智慧商城项目实战

首页、分类、详情、登录、购物车、用户、订单等功能模块实战

- 开发流程
- Git 工作流
- PingCode 管理平台
- Vant 组件库
- npm 支持
- computed 框架扩展
- Mobx 状态管理
- ...
