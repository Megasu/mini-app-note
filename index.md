---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: 微信小程序
  tagline: 黑马程序员出品
  image: /logo.png
  actions:
    - theme: brand
      text: 小程序基础
      link: /mini-app/
    - theme: alt
      text: 🥇享+生活项目
      link: /enjoy-plus/
    - theme: alt
      text: 🥈智慧商城项目实战
      link: /wisdom-shop/

features:
  - icon: 🎯
    title: 小程序基础
    details: 组件、配置、生命周期、事件处理、数据渲染、API、自定义组件、Vant UI、分包加载等
  - icon: 🥇
    title: 享+生活项目
    details: 通告管理、用户管理、房屋管理、报修管理、访客管理等各个功能模块
  - icon: 🥈
    title: 智慧商城项目实战
    details: 首页、分类、详情、登录、购物车、用户、订单等功能模块实战
---
